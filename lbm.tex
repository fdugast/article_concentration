
\section{Lattice Boltzmann method}
\label{sec:lbm}

\subsection{Multi-relaxation time model for the fluid flow}


The lattice Boltzmann method (LBM) is an alternative way of solving fluid flow problems compared to classical methods based on the discretization of the Navier--Stokes equations (finite volume or finite element methods). This method is based on the calculation of the distribution function $f$ involved in the Boltzmann equation and which gives the probability of finding a particle at the position $\bx$, at the time $t$ and at the speed $\boldsymbol{c}$. Compared to the Boltzmann equation, the velocity vector is discretized along directions in the lattice Boltzmann equation, such as~\cite{Wolf-Gladrow2000a,Succi2001}: 
%
\begin{equation} \label{Boltzmann continu}
\frac{\partial f_i}{\partial t} + \bc_i \cdot \nabla f_i  = \Omega_i (\boldsymbol{f}) \quad\forall i\in\llbracket0;I-1\rrbracket.
\end{equation}

The notation $\boldsymbol f=\{f_i\}_{i=0}^{I-1}$ will be used for the distribution function. Such a notation will be used for other quantities, as soon as there is no possible ambiguity.  
In two dimensions, the reference discretization scheme involves $I=9$ discrete directions --~yielding the so-called D2Q9 scheme. The directions and associated weights are given by~\cite{Qian1992}:
%
\begin{equation}
\bc=
\begin{pmatrix}
0 & 1 & 0 & -1 & 0 & 1 & -1 & -1 & 1\\
0 & 0 & 1 & 0 & -1 & 1 & 1 & -1 & -1
\end{pmatrix},
\end{equation}
%
\begin{equation}
\boldsymbol{w}=
\begin{pmatrix}
\frac{4}{9} & \frac{1}{9} & \frac{1}{9} & \frac{1}{9} & \frac{1}{9} & \frac{1}{36} & \frac{1}{36} & \frac{1}{36} & \frac{1}{36}
\end{pmatrix}.
\end{equation}


Also, from the definition of the velocity directions, it is convenient to introduce the opposed velocity directions $\boldsymbol{\bar{i}}$ with respect to $\boldsymbol{c}_0$, to write the bounce-back boundary conditions:
%
\begin{equation}
\boldsymbol{\bar{i}}=
\begin{pmatrix}
\bar 0 & \bar 1 & \bar 2 & \bar 3 & \bar 4 & \bar 5 & \bar 6 & \bar 7  & \bar 8
\end{pmatrix}=
\begin{pmatrix}
0 & 3 & 4 & 1 & 2 & 7 & 8 & 5 & 6
\end{pmatrix}.
\end{equation}


The lattice Boltzmann equation~\eqref{Boltzmann continu} is generally solved in two steps: the collision step, and the streaming step. For the collision operator,
$\Omega(\boldsymbol{f})$, the simple model introduced by Bhatnagar-Gross-Krook (BGK)~\cite{Bhatnagar1954,Marie2008} which relies on a single-relaxation time (SRT) is often used.
However, this is often a source of spurious oscillations and instabilities as the Reynolds number increases. As an example, d'Humi\`eres et al.~\cite{DHumieres2002} reported severe oscillations for the pressure field in the cavity flow for the SRT model at Re=500. So, in order to improve the numerical stability of the LBM, a multi-relaxation time (MRT) operator has further been introduced~\cite{DHumieres2002}. It is written as:
%
\begin{equation}
\Omega (\boldsymbol{f}) = -\mathbf{M}^{-1}\mathbf{S}\mathbf{M} 
\left(\boldsymbol{f}-\boldsymbol{f}^{\text{eq}}\right),
\end{equation} 
%
in which $\mathbf{M}$ is the transformation matrix between the moments and the distribution functions:
%
\begin{equation}
\mathbf{M}=
\begin{pmatrix}
\phantom{-}1 & \phantom{-}1 & \phantom{-}1 & \phantom{-}1 & \phantom{-}1 & \phantom{-}1 & \phantom{-}1 & \phantom{-}1 & \phantom{-}1\\
-4 & -1 & -1 & -1 & -1 & \phantom{-}2 & \phantom{-}2 & \phantom{-}2 & \phantom{-}2\\
\phantom{-}4 & -2 & -2 & -2 & -2 & \phantom{-}1 & \phantom{-}1 & \phantom{-}1 & \phantom{-}1\\
\phantom{-}0 & \phantom{-}1 & \phantom{-}0 & -1 & \phantom{-}0 & \phantom{-}1 & -1 & -1 & \phantom{-}1\\
\phantom{-}0 & -2 & \phantom{-}0 & \phantom{-}2 & \phantom{-}0 & \phantom{-}1 & -1 & -1 & \phantom{-}1\\
\phantom{-}0 & \phantom{-}0 & \phantom{-}1 & \phantom{-}0 & -1 & \phantom{-}1 & \phantom{-}1 & -1 & -1\\
\phantom{-}0 & \phantom{-}0 & -2 & \phantom{-}0 & \phantom{-}2 & \phantom{-}1 & \phantom{-}1 & -1 & -1\\
\phantom{-}0 & \phantom{-}1 & -1 & \phantom{-}1 & -1 & \phantom{-}0 & \phantom{-}0 & \phantom{-}0 & \phantom{-}0\\
\phantom{-}0 & \phantom{-}0 & \phantom{-}0 & \phantom{-}0 & \phantom{-}0 & \phantom{-}1 & -1 & \phantom{-}1 & -1\\
\end{pmatrix},
\end{equation}
%
and $\mathbf{S}$ is the diagonal relaxation rate matrix defined by~\cite{Liu2014e}:
%
\begin{equation}
\mathbf{S}=\text{diag}(1,s_e,s_\epsilon,1,s_q,1,s_q,s_\nu,s_\nu).
\end{equation}

Note that $s_i=1$ corresponds to conserved moments (conservation of mass and momentum), and $s_\nu$ is related to the fluid viscosity:
%
\begin{equation}
\label{nu}
\frac{1}{s_\nu}=3\nu +\frac{1}{2}.
\end{equation}

The other components are to be adjusted in order to improve the numerical stability and are often determined to be close to one through an optimization process~\cite{Tekitek2006,Xu2012}. In this article the fluid used in the cavity is water, so an incompressible model is used for the LBM. The equilibrium distribution $f_i^{\text{eq}}$ is then given by~\textcolor{red}{\cite{Zou1995a,He1997c}}: 
%
\begin{equation}\label{feq_incomp} 
 f_{i}^{\text{eq}}=\omega_{i} \left(\rho +\rho_{0}\left(3\alpha(\bc_{i}\cdot\bu) +\frac{9}{2}\alpha(\bc_{i}\cdot\bu)^{2} - \frac{3}{2}\alpha\bu^{2} \right)\right).
\end{equation}

The design-related variable $\alpha(\psi(\bx))$ involved in~\cref{feq_incomp} has been defined in~\cref{sec:optPbDefinition}.  The macroscopic quantities, the fluid density $\rho$, the 
velocity $\bu$, and the pressure can further be computed with the moments of $\bff$:
%
\begin{equation}
\rho(\bx,t) = \sum_{i=0}^{8} f_{i}(\bx,t), \quad
\rho_0\bu(\bx,t) = \sum_{i=0}^{8} \bc_{i} f_{i} (\bx,t), \quad \color{black}
p(\bx,t)= c_{s}^{2} \rho (\bx,t),
\end{equation}
%
where $\rho=\rho_0+\delta\rho$, $\delta\rho$ being the density fluctuation and \textcolor{red}{ the mean density $\rho_0$ is chosen to be equal to 1 in our simulations~\cite{He1997c}}, and $c_{s}=1/\sqrt{3}$ the lattice speed of sound. The fluid velocity needs to be very low compared to $c_{s}$ in order to satisfy the low mach number assumption required for the equilibrium distribution function expansion~\cite{Chen1998a}.  \textcolor{red}{Compared to the classical model, neglecting the density fluctuations in terms involving velocity induces a decoupling between density and velocity~\cite{Lallemand2000a}. While reducing the errors due to compressibility~\cite{Mei2006}, it should be noted that this model is only valid for steady-state flows~\cite{Dellar2014}.}
Next, the bounce-back boundary condition is applied on all interfaces but on the inlet and outlet, where the boundary conditions proposed by Zou and He~\cite{Zou1997} are applied. 
The user-parameter $\epsilon$ involved in~\eqref{eq:Psi2alphaRegul} is chosen to tend to zero within this multi-relaxation time lattice Boltzmann model, so that $\alpha$ equals either 0 or 1, and a clear definition of the topology is considered to avoid any bias in the fluid flow forward modeling.



\subsection{Single-relaxation time model for the concentration distribution}

Based on a double distribution function approach, a second distribution function, $\bgg$, similar to $\bff$, is introduced to solve the concentration field ~\cite{Yan2008,Li2017}:
%
\begin{equation} 
\frac{\partial g_i}{\partial t} + \bc_i \cdot \nabla g_i  + s_{i}= \Omega_i
\quad \forall i\in\llbracket0;I-1\rrbracket, 
\label{lbm_g}
\end{equation}
%
with the collision operator defined as:
%
\begin{equation}
\Omega_i = -\frac{1}{\tau_g}\left(g_i-g_i^{\text{eq}}\right).
\end{equation}

Although the multi-relaxation time collision operator is used for the distribution function $\bff$, the simpler single-relaxation time model is used here, with only one relaxation time $\tau_g$ for the distribution function $\bgg$. The lattice Boltzmann equation for the concentration is more stable than the one for the fluid flow, and the multi-relaxation time model is not mandatory for this distribution function.Indeed, the SRT-BGK model is suitable to solve this problem as long as the diffusion is isotropic~\cite{Chai2016,Rasin2005}, which is the case here. The relaxation time $\tau_g$ is related to the mass diffusivity $D$ of the chemical species:
%
\begin{equation}
\tau_g=3D+\frac{1}{2}.
\end{equation}

In~\eqref{lbm_g}, the source term involved in the reaction process equation is distributed on all the discrete velocity directions, according to the D2Q9 scheme. For a linear source term, $s=k\,\conc$, one can write:
%
\begin{equation}
s_i=\omega_i\,k\,\conc\quad\forall i\in\llbracket0;I-1\rrbracket.
\end{equation}
More generally, the LBM formulation for  a given source model $s$ is relaxed towards the different velocities directions as follow:
%
\begin{equation}
s_i=\omega_i\,s.
\end{equation}

The equilibrium distribution function $g_i^{\text{eq}}$ is given by~\cite{Pingen2009c}:
%
\begin{equation}
g_i^{eq}=\omega_i\, \conc\left(1+3\alpha\ \bc_i \cdot \bu\right).
\end{equation}

It can be noticed that the velocity field is used to compute this equilibrium distribution function, illustrating the coupling between the concentration and the velocity.The concentration $\conc$ is computed with the zeroth order of the distribution function $\bgg$ such as~\cite{Li2017} :

\begin{equation}
\conc(\boldsymbol{x},t)=\sum_{i=0}^8{g_i(\boldsymbol{x},t)}
\label{dirichlet}
\end{equation}

The mass flux is written as~\cite{Yaji2016}:

\begin{equation}
\boldsymbol{q}(\boldsymbol{x},t)=\sum_{i=0}^8{\boldsymbol{c}_i g_i(\boldsymbol{x},t) - \conc \boldsymbol{u}}
\label{neumann}
\end{equation}

In order to implement the boundary conditions, the unknown distribution functions are assumed to be the equilibrium distribution functions with the concentration $\conc^{'}$ to be determined~\cite{Inamuro2002,DUGAST2018}. For the prescribed inlet concentration,  $\conc^{'}$ is calculated by~\eqref{dirichlet}, with ${\conc}={\conc}_{\text{in}}$. At the outlet,  $\conc^{'}$ is calculated by~\eqref{neumann}, with $\boldsymbol{q}=0$. \\

A similar solver has been validated for a fluid flow and heat transfer problem by the authors~\cite{FlorianThese}. In that case, the passive scalar approach is also used, the only difference is that the concentration is replaced by the temperature.



\subsection{Residual form}

In order to calculate the cost function gradient, the adjoint-state method will be used, see~\cref{sec:gradient}.  In order to set it up, the forward problem, which consists of both the multi-relaxation time model for the fluid-flow, and the single-relaxation time model for the concentration distribution, is written down in terms of residuals. We denote $R$ and $P$ the residuals for the Boltzmann state equations, and the boundary conditions, respectively. Further, we use the superscripts ${f}$ and ${g}$ for the fluid flow, and for the concentration, respectively. Such residuals have to be written down for each velocity direction. Added to that, the boundary $\partial\mathcal{D}$ of the medium $\mathcal{D}$ is partitioned with the inlet $\mathcal{D}_{\text{in}}$, the outlet $\mathcal{D}_{\text{out}}$, and the no-slip boundaries $\mathcal{D}_{\text{ns}}$. 
With such notations, the forward problem is concisely written as:
%
\begin{equation}\label{eq:fwPbLBM}
\left\|\begin{array}{ll}
\text{search } (f_i,g_i)(\bx,t),\  i\in\llbracket 0,...,8\rrbracket \text{ such that:}  \\
\bullet\ \forall\bx\in\mathcal D: \\
\phantom{\bullet}\ R^{f}_{i}(\bff,\alpha)=\frac{\partial f_{i}}{\partial t}+\bc_{i} \cdot \nabla f_{i}+\left(\mathbf{M}^{-1}\mathbf{S}\mathbf{M} 
\left(\bff-\bff^{\text{eq}}\right)\right)_i=0,\\
\phantom{\bullet}\ R^{g}_{i}(\bff,\bgg,\alpha)=\frac{\partial g_{i}}{\partial t}+\bc_{i} \cdot \nabla g_{i}+s_i+\frac{1}{\tau_g} 
\left({g}_i-{g}_i^{\text{eq}}\right)=0,  \\
\\
\bullet\ \forall\bx\in\partial\mathcal{D}_{\text{in}}: 											\\
\phantom{\bullet}\ P^f_{1}(\bff)=f_{1}-f_{3}-\frac{2}{3} u_{\text{in}} =0,  		\\
\phantom{\bullet}\ P^f_{5}(\bff)=f_{5}-f_{7}-\frac{1}{6} u_{\text{in}}-\frac{1}{2}(f_{4}-f_{2}) =0,  														\\
\phantom{\bullet}\ P^f_{8}(\bff)=f_{8}-f_{6}-\frac{1}{6} u_{\text{in}}+\frac{1}{2}(f_{4}-f_{2}) =0,   													\\
\phantom{\bullet}\ P^g_1(\bgg)=g_{1}-\frac{1}{9}\chi\left(1+3 u_{\text{in}}\right)=0, 		\\
\phantom{\bullet}\ P^g_5(\bgg)=g_{5}-\frac{1}{36}\chi\left(1+3 u_{\text{in}}\right)=0, 		\\
\phantom{\bullet}\ P^g_8(\bgg)= g_{8}-\frac{1}{36}\chi\left(1+3u_{\text{in}}\right)=0, 		\\
\\
\bullet\ \forall\bx\in\partial\mathcal{D}_{\text{out}}:   										\\
\phantom{\bullet}\ P^f_3(\bff)= f_{3}-f_{1}+\frac{2}{3}\eta=0,  										\\
\phantom{\bullet}\  P^f_6(\bff)=f_{6}-f_{8}+\frac{1}{6} \eta-\frac{1}{2}\left(f_{4}-f_{2}\right)=0, 	\\
\phantom{\bullet}\  P^f_7(\bff)=f_{7}-f_{5}+\frac{1}{6}\eta+\frac{1}{2}\left(f_{4}-f_{2}\right)=0,		\\
\phantom{\bullet}\  P^g_3(\bff, \bgg)= g_{3}-\frac{1}{9}\zeta\left(1-3\eta\right)=0,				\\
\phantom{\bullet}\  P^g_6(\bff, \bgg)=g_{6}-\frac{1}{36}\zeta\left(1-3\eta\right)=0,			\\
\phantom{\bullet}\  P^g_7(\bff, \bgg)=g_{7}-\frac{1}{36}\zeta\left(1-3\eta\right)=0,	 		\\
\\
\bullet\ \forall\bx\in\partial\mathcal{D}_{\text{ns}}:   	\\
\phantom{\bullet}\ P^f_{(\boldsymbol{c}_{i}\cdot \boldsymbol{n} < 0)}(\bff)= f_{(\boldsymbol{c}_{i}\cdot \boldsymbol{n} < 0)}-f_{(\boldsymbol{c}_{\textcolor{red}{\bar{i}}}\cdot \boldsymbol{n} > 0)}=0,	\\
\phantom{\bullet}\ P^g_{(\boldsymbol{c}_{i}\cdot \boldsymbol{n} < 0)}(\bgg)= g_{(\boldsymbol{c}_{i}\cdot \boldsymbol{n} < 0)}-g_{(\boldsymbol{c}_{\textcolor{red}{\bar{i}}}\cdot \boldsymbol{n} > 0)}=0,	
\end{array}\right.\end{equation}
%
with:
%
\begin{equation}
\begin{array}{l}
\eta=-\rho_{\text{out}}+f_{0}+f_{2}+f_{4}+2(f_{1}+f_{5}+f_{8}), \\
\chi=\frac{6\left({\conc}_{\text{in}}-g_{0}-g_{2}-g_{3}-g_{4}-g_{6}-g_{7}\right)}{\left(1+3u_{\text{in}}\right)},\\
\zeta=\frac{6 \left(g_{1}+g_{5}+g_{8}\right)}{\left(1+3\eta\right)}.
\end{array}
\end{equation}

%modif c'est uin pour l'entree, eta c'est uout



\subsection{LBM-based cost function}

The cost function written in terms of the primal variables is given combining~\cref{eq:cost,eq:Jplus}, which gives: 
%
\begin{equation}\label{eq:costJplusBis}
\mathcal{J}^+\left(\bu,\conc,\Delta p\right)=
\frac{1}{\left|\partial\mathcal D_{\text{out}}\right|}\int_{\partial\mathcal D_{\text{out}}}{\bu\cdot\bn\ \conc\ \mathrm{d}\bx}
+\ell \Delta p_{\max} \exp\left(\frac{\Delta p}{\Delta p_{\max}}\right).
\end{equation}

This cost function is to be re-written in terms of the lattice Boltzmann variables, $\bff$ and $\bgg$. Taking into account of the following relationships between the primal variables and the lattice Boltzmann variables:
%
\begin{equation}
\begin{array}{l}
\bu=\sum_{i=0}^{8} \bc_{i} f_{i} (\bx,t) \\[3pt]
\conc=\sum_{i=0}^{8} g_{i} (\bx,t)\\[3pt]
\Delta p=\frac{1}{\left|\partial\mathcal D_{\text{in}}\right|}\left[\frac{1}{3}\int_{\partial\mathcal{D}_{\text{in}}}{\sum_{i=0}^{8}f_{i} \, \md \bx}-\frac{1}{3}\int_{\partial\mathcal{D}_{\text{out}}}{\sum_{i=0}^{8}f_{i}}  \, \md \bx\right],
\end{array}
\end{equation}
%
the cost function that is, finally, to be minimized is:
%
\begin{equation}\label{eq:costJplusTer}
\begin{split}
&\widehat{\mathcal{J}}^+\left(\bff,\bgg\right)=
\frac{1}{\left|\partial\mathcal D_{\text{out}}\right|}\int_{\partial\mathcal D_{\text{out}}}{\sum_{i=0}^{8} \bc_{i} f_{i} \cdot\bn\ \sum_{i=0}^{8} g_{i} \ \mathrm{d}\bx}\\
&+\ell \Delta p_{\max} \exp\left(\frac{\frac{1}{3}\int_{\partial\mathcal{D}_{\text{in}}}{\sum_{i=0}^{8}{f_{i}} \,\md \bx}}{\left|\partial\mathcal D_{\text{in}}\right| \Delta p_{\max}}\right).
\end{split}
\end{equation}

Note that the pressure term at the outlet is not involved here. This one being considered as prescribed, the pressure difference is only driven by the modification of the inlet pressure during the optimization.










