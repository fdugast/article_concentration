
\section{Definition of the optimization problem} \label{sec:optPbDefinition}

\subsection{The physics of advection-diffusion-reaction}
\label{sec:physics}

The chemical species are transported by the fluid flow and the reaction is taken into account by a source term involved in the equation of the species concentration. This problem is written with the following macroscopic equations $\forall\bx\in\mathcal D$, $\mathcal D$ being an open bounded set of $\mathbb{R}^2$:
%
\begin{align}
\nabla \cdot \bu=0 , \label{eq:NS1}\\
\left(\bu\cdot \nabla \right)\bu+\frac{1}{\rho}\nabla p-\nu \nabla^{2}\bu=0, \label{eq:NS2}\\
\bu\cdot \nabla \conc-D\nabla^{2}\conc+s=0. \label{eq:NS3}
\end{align}

The source term $s$ involved in~\cref{eq:NS3} is expressed as a function of the concentration of the species of concern, $\conc=[A]=\frac{n_A}{V}$. One possibility is  to express a linear dependency between $s$ and $\conc$, as in~\cite{Kim2012}. Elsewhere, in~\cite{Schapper2011} and~\cite{Yaji2018a}, the reaction rate is limited by the ethanol production, or by the velocity field, meaning that the reaction rate reaches a maximum value $k$ for a specific concentration. Many other models can be used depending on the chemical reactions, using linear, quadratic, cubic, logarithmic, or exponential relations, to cite but a few~\cite{Dawson1993,Weimar1996}. With a linear source term, $s=k\,\conc$, when introducing a given characteristic length of the computational domain $L$, and a characteristic velocity ${U}$ into the initial set of equations \crefrange{eq:NS1}{eq:NS3}, and combining the three physical parameters, namely the fluid viscosity, $\nu$, the mass diffusivity of the chemical species, $D$, and the maximal reaction rate, $k$, one can recover the classical dimensionless version of the initial problem:

\begin{align}
\widetilde{\nabla} \cdot \widetilde{\bu}=0 , \label{eq:NSa1}\\
\left(\widetilde{\bu}\cdot \widetilde{\nabla} \right)\widetilde{\bu}+\widetilde{\nabla} \widetilde{p}-\frac{1}{\text{Re}} \widetilde{\nabla}^{2}\widetilde{\bu}=0, \label{eq:NSa2}\\
\widetilde{\bu}\cdot \widetilde{\nabla} \widetilde{c}_A-\frac{1}{\text{Pe}}{\widetilde{\nabla}}^{2}\widetilde{c}_A+\text{Da}\,\widetilde{c}_A=0. \label{eq:NSa3}
\end{align}

Here tilde values $\widetilde{X}$ stand for dimensionless version of ${X}$ and the three dimensionless numbers $\text{Re}$, $\text{Sc}$ and $\text{Da}$ are defined as follow:
%
\begin{itemize}
	\item the Reynolds number, $\text{Re}=\frac{UL}{\nu}$, represents the ratio between inertial and viscous forces involved in a fluid flow. It can help to distinguish flow regimes, from laminar to turbulent flows;
	\item the Schmidt number, $\text{Sc}=\frac{\nu}{D}$, is the ratio of the kinematic viscosity (momentum diffusivity) over the mass diffusivity;
	\item the Damkohler number, $\text{Da}=\frac{kL}{U}$, represents the ratio of the reaction rate over the transport phenomena rate in the system.
\end{itemize}

Alternatively,  the Peclet number can be used, $\text{Pe}=\text{Re}\times\text{Sc}$. It represents the ratio between the transfer by convection and the transfer by diffusion.

In fluid flow topology optimization problems, the principal effect of modifying the topology is the modification of the velocity distribution. It means that for the optimization to be effective, the convection process has to play a major role for the concentration distribution when compared to the diffusion process. Otherwise, the influence of the advection process on the efficiency of the chemical reaction would be too small. So a high Peclet number is chosen here to respect this condition. However, diffusion dominant processes can also be optimized using this method  but the problem will be simpler  by removing \crefrange{eq:NS1}{eq:NS2} to the set of equations to be solved. About the reaction rate, a compromise is to be found: if the reaction rate represented by the Damkohler number is too high, the reactant is consumed too fast in the domain, and, there is no need of using any topology optimization algorithms. Completely on the other hand, if the reactant is consumed too slowly, then the concentration gradients are too small and the impact of the fluid/solid geometry will also be negligible. For these two extreme cases, the optimization problem is insensitive because of the bad sizing of the space domain $\mathcal D$ : this one has to be decreased (resp increased) in order to improve the sensitivity of the physical problem to the topology of the domain. In all cases in between, for ordinary reaction rates, the use of topology optimization may be very impact-full.






\subsection{Setting up the optimization problem: reaction maximization}
\label{sec:optPbsettingUp}

In topology optimization, one searches for the best material distribution --~or at least a good enough distribution~-- to satisfy a given objective. {The material distribution is represented by the design variable, and the objective is defined in terms of a cost function,  $\mathcal{J}$. The update of the geometry will be obtained by the evolution of a level-set function, involving the cost function gradient. This gradient is given by the derivative of the cost function $\mathcal{J}$ with respect to the design variable.} \Cref{sec:gradient} is dedicated to such a derivation. The objective of the optimization problem is the maximization of the reaction, which is equivalent to the  minimization of the product of the velocity with the concentration, at the outlet of the domain. Mathematically, this is written as:
%
\begin{equation}\label{eq:cost}
\mathcal{J}=
\frac{1}{\left|\partial\mathcal D_{\text{out}}\right|}\int_{\partial\mathcal D_{\text{out}}}{\bu\cdot\bn\ \conc\ +D \nabla \conc\cdot\bn\ \mathrm{d}\bx},
\end{equation}
%
where $\bn$ is the outward unit normal vector to the boundary, the first term of the integral is the convective part of the reactant flux while the second term represents the diffusive component. As the Pe number is far greater than one in all simulations, the diffusive component is neglected in this work.

A constraint is also to be added to the optimization problem in order to allow a maximum amount of pressure drop during the optimization process. Such a limitation is useful to prevent from unfeasible designs. Rather than adding inequality constraints on the optimization problem, a penalization term is added to the cost function, as in~\cite{allaire2006structural}, in structural optimization, for example. The augmented cost function, $\mathcal{J^{+}}$, to be minimized, is thus:
%
\begin{equation}\label{eq:Jplus}
\mathcal{J^{+}}= \mathcal{J}+\ell\mathcal{J}_{1} \quad \text{with} \quad \mathcal{J}_{1}=\Delta p_{\max} \exp\left(\frac{\Delta p}{\Delta p_{\max}}\right),
\end{equation}
%
and $\ell \in \mathbb{R^{+}}$ is a user-defined value controlling the relative importance of the different contributions $\mathcal{J}$ and $\mathcal{J}_{1}$.


\subsection{Geometry update}

The design domain is divided into $N$ elements and there is one design parameter per element. {The topology is  represented by the signed variable ${\alpha}(\bx)\in\mathbb{R}^N$.} Then the level-set method is used for the mapping of the design variable. Such a method has been firstly implemented for the representation of interfaces in multiphase flows~\cite{Osher1988} and for image segmentation~\cite{Osher2003}. It has then been used for topology optimization, by Sethian~et~al.~\cite{Sethian2000}, Wang~et~al.~\cite{Wang2003}, and Allaire~et~al.~\cite{Allaire2005}.

{The continuous level-set function, is such that its zero contour defines the interface between the two media.  The update of the solid/fluid distribution $\alpha$ is thus performed in two steps. The first one is via the evolution of the discrete level-set function $\Psi(\bx)\in\mathbb{R}^N$:}
%
\begin{equation}\label{eq:PsiEvol} 
\Psi^{(n+1)}(\bx)=\Psi^{(n)}(\bx)-\bold{P}^{(n)} {\nabla_\Psi\mathcal{J}^{+}}^{(n)}(\bx).
\end{equation}

The superscript is the iteration count,
{$\bold{P}$ is the iteration matrix, ideally a good   approximation of the inverse of the Hessian matrix, $\bold{P}\approx\widetilde{\bold{H}}^{-1}$}, and $\nabla_\Psi\mathcal{J}^+$ is the augmented cost function gradient (this vector contains the derivatives of the cost function with respect to all components of $\Psi$). The second step is the topology update which consists in applying the mapping between the level-set function and the topology. 

In a previous study, the mapping from the level set function to the topology was defined as~\cite{DUGAST2018}:
%
\begin{equation}\label{eq:Psi2alpha}
\alpha(\Psi(\bx))=\frac{1}{2}\left(1+\text{sign}\Psi(\bx)\right),
\end{equation}
%
such that
%
\begin{equation}\label{eq:alphaPsi}
\alpha(\bx)=
\begin{cases}
0 & \text{if }\Psi(\bx) < 0;  \\
1 & \text{if }\Psi(\bx) > 0. \\
\end{cases}
\end{equation}

A smooth version of the mapping can be considered, for example~\cite{aghasi2011parametric}:
%
\begin{equation}\label{eq:Psi2alphaRegul}
\alpha_\epsilon(\Psi(\bx))=\frac{1}{2} + \frac{1}{\pi}\arctan\frac{\Psi(\bx)}{\epsilon}.
\end{equation}

The main interest in using~\eqref{eq:Psi2alphaRegul} rather than~\eqref{eq:Psi2alpha} is that the former is a continuous and differentiable function, with $\alpha'_\epsilon(\Psi)$ finite everywhere, as soon as $\epsilon\neq0$. 
Besides, if $\epsilon\to0$, a clear discontinuous fluid/solid interface is obtained, which is useful while solving the forward problem. Indeed, the LBM implementation used here for the fluid flow and for the concentration field does not involve a force term to deal with a Brinkmann penalization for a porous media. As a consequence, each node must be either strictly fluid or solid, and no intermediate state is allowed. It means that a threshold is applied after the update of the level-set function to obtain a clear fluid/solid interface for the forward problem. The choice of this parameter $\epsilon$ is discussed in the section dedicated to numerical results. In the following, the subscript $\epsilon$ is avoided for readability considerations.


Note that other strategies such as the Brinkmann penalization for modeling the porous medium, coupled with the SIMP method could have been used~\cite{Makhija2012}. However, the level-set function allows a clearer definition of the topology, and thus minimizes the possible bias involved within the forward model.








