
\section{The cost function gradient}\label{sec:gradient}


In order to derive the full adjoint-state problem needed for the computation of the cost function gradient, the methodology and notations of Gunzburger~\cite{Gunzburger2000} are introduced:
%
\begin{itemize}
	\item $\bF(\bphi,{\Psi})=0$ is the forward model gathering all the equations of~\cref{eq:fwPbLBM};
	\item $\bphi=\left(\bff,\bgg\right)$ is the global forward state;
	\item $\bphi^*=\left(\bff^*,\bgg^*\right)$ is the global adjoint-state.
\end{itemize}

The Lagrange function associated to the optimization problem is written as\footnote{With the notations given above, this~\cref{eq:lagrange} can be straitforwardly expanded to:
	%
	\begin{multline}
	\mathcal L(\bff,\bgg,{\Psi},\bff^*,\bgg^*)=\widehat{\mathcal{J}}^{+}(\bff,\bgg)
	+\int_{0}^{t_{f}}  \Bigg( 
	\int_{\mathcal D} \,\sum_{\substack{q=\{f,g\} \\ i=0,...,8 }} R_{i}^q\ q_{i}^{*}\ \md\bx	
	\\
	+\bigg(
	\int_{\partial\mathcal D_{\text{in}}} \,\sum_{\substack{q=\{f,g\} \\ i=\{1,5,8 \} } } 
	+\int_{\partial\mathcal D_{\text{out}}} \,\sum_{\substack{q=\{f,g\} \\ i=\{3,6,7 \} } } 
	+ \int_{\partial\mathcal D_{\text{ns}}} \,\sum_{\substack{q=\{f,g\} \\ \bc_i\cdot\bn<0 } } \bigg) \left(P_{i}^q\ q_{i}^{*}\ \md\bx \right)
	\ \Bigg) \md t.
	\end{multline}
}:
%
\begin{equation}\label{eq:lagrange}
\mathcal L\left(\bphi,{\Psi},\bphi^*\right)=\widehat{\mathcal{J}}^+(\bphi)+\left<\bF(\bphi,{\Psi}),\bphi^*\right>.
\end{equation}

The optimization problem is solved searching for the stationary point of the Lagrange function. This point satisfies:
%
\begin{equation}\label{eq:lagrangeDifferential}
\delta\mathcal{L}=\frac{\partial\mathcal L}{\partial\bphi} \delta\bphi 
				 +\frac{\partial\mathcal L}{\partial{\Psi}} \delta{\Psi}
				 +\frac{\partial\mathcal L}{\partial\bphi^*} \delta\bphi^*=0,
\end{equation}
%
where $\delta\mathcal L$, $\delta\bphi$, $\delta{\Psi}$ and $\delta\bphi^*$ stand for arbitrary variations~\cite{Gunzburger2000}. Three terms appear in the right hand side of~\eqref{eq:lagrangeDifferential}:
%
\begin{itemize}
%
\item the first term, the differential with respect to the state, $\partial\mathcal L/\partial\bphi$, yields the adjoint-state equation;
%
\item the second term, the differential with respect to the design variables, $\partial\mathcal L/\partial{\Psi}$, yields the optimality conditions;
%
\item the third term, the differential with respect to the adjoint-state variable, $\partial\mathcal L/\partial\bphi^*$, yields back the equations of the forward model that are to be satisfied.
%
\end{itemize}

A similar derivation of the adjoint-state problem for a convective fluid flow problem has been detailed by the authors~\cite{DUGAST2018}. Based on this method, the adjoint-state is finally written as :

%
\begin{equation}\label{eq:bwPbLBM}
\left\|\begin{array}{ll}
\text{search } (f_i^*, g_i^*)(\bx,t),\  i\in\llbracket 0,...,8\rrbracket, \text{ with } (f_i,g_i)(\bx,t) \text{ given by \eqref{eq:fwPbLBM}},\, \text{ such that:}  \\
\bullet\ \forall\bx\in\mathcal D: \\
\phantom{\bullet}\ R^{*,f}_{i}(\bff,\bgg, \bff^*,\bgg^*,\alpha)=-\frac{\partial f_{i}^*}{\partial t}-\bc_{i} \cdot \nabla f_{i}^*+\left(\mathbf{M}^{-1}\mathbf{S}\mathbf{M} 
\left(\bff^*-\bff^{*,\text{eq}}\right)\right)_i+Q_i^{*,f}=0,  \\
\phantom{\bullet}\ R^{*,g}_{i}(\bff,\bgg, \bgg^*,\alpha)=-\frac{\partial g_{i}^*}{\partial t}-\bc_{i} \cdot \nabla g_{i}^*+\frac{1}{\tau_g} \left(g_i^*-g_i^{*,eq}\right)+Q_i^{*,g}=0,  \\
\\
\bullet\ \forall\bx\in\partial\mathcal{D}_{\text{in}}: 																							\\
\phantom{\bullet}\ P^{*,f}_{3}(\bff^*,\bff)=f_{3}^*-f_{1}^*+\ell \,\frac{1}{3 \left|\partial\mathcal D_{\text{in}}\right|}  \exp\left(\frac{\frac{1}{3}\int_{\partial\mathcal{D}_{\text{in}}}{\sum_{i=0}^{8}{f_{i}} \,\md \bx}}{\left|\partial\mathcal D_{\text{in}}\right| \Delta p_{max}}\right) 																				\\ 
\phantom{\bullet}\ P^{*,f}_{7}(\bff^*,\bff)=f_{7}^{*}-f_{5}^{*}+\ell  \,\frac{1}{3 \left|\partial\mathcal D_{\text{in}}\right|}  \exp\left(\frac{\frac{1}{3}\int_{\partial\mathcal{D}_{\text{in}}}{\sum_{i=0}^{8}{f_{i}} \,\md \bx}}{\left|\partial\mathcal D_{\text{in}}\right|\Delta p_{max}}\right)  																																		\\ 
\phantom{\bullet}\ P^{*,f}_{6}(\bff^*,\bff)=f_{6}^{*}-f_{8}^{*}+ \ell  \,\frac{1}{3 \left|\partial\mathcal D_{\text{in}}\right|}  \exp\left(\frac{\frac{1}{3}\int_{\partial\mathcal{D}_{\text{in}}}{\sum_{i=0}^{8}{f_{i}}\, \md \bx}}{\left|\partial\mathcal D_{\text{in}}\right| \Delta p_{max}}\right)  											  																						\\ 
\phantom{\bullet}\ P^{*,g}_3(\bgg^*)= g^*_3+\frac{1}{6}\left(4g^*_1+g^*_5+g^*_8\right)															\\
\phantom{\bullet}\ P^{*,g}_6(\bgg^*)= g^*_6+\frac{1}{6}\left(4g^*_1+g^*_5+g^*_8\right)															\\
\phantom{\bullet}\ P^{*,g}_7(\bgg^*)= g^*_7+\frac{1}{6}\left(4g^*_1+g^*_5+g^*_8\right)															\\ 
\\
\bullet\ \forall\bx\in\partial\mathcal{D}_{\text{out}}:   																						\\
\phantom{\bullet}\ P^{*,f}_1(\bff^*,\bgg^*,\bff, \bgg) = f^*_1 - f^*_3 + \frac{1}{3}\left(4f^*_3+f^*_6+f^*_7\right) + \frac{1}{6}\left(\frac{\zeta}{1+3\eta}\right)\left(4g^*_3+g^*_6+g^*_7\right)+\frac{\conc(\bgg)}{\left|\partial\mathcal{D}_{\text{out}}\right|}=0														\\
\phantom{\bullet}\ P^{*,f}_5(\bff^*,\bgg^*,\bff, \bgg)=f^*_5 - f^*_7 + \frac{1}{3}\left(4f^*_3+f^*_6+f^*_7\right) + \frac{1}{6}\left(\frac{\zeta}{1+3\eta}\right)\left(4g^*_3+g^*_6+g^*_7\right)+\frac{\conc(\bgg)}{\left|\partial\mathcal{D}_{\text{out}}\right|}=0														\\
\phantom{\bullet}\ P^{*,f}_8(\bff^*,\bgg^*,\bff, \bgg)=f^*_8 - f^*_6 + \frac{1}{3}\left(4f^*_3+f^*_6+f^*_7\right) + \frac{1}{6}\left(\frac{\zeta}{1+3\eta}\right)\left(4g^*_3+g^*_6+g^*_7\right)+\frac{\conc(\bgg)}{\left|\partial\mathcal{D}_{\text{out}}\right|}=0														\\
\phantom{\bullet}\ P^{*,g}_1(\bgg^*,\bff)= g^*_1-\frac{1}{6}\left(\frac{1-3\eta}{1+3\eta}\right)\left(4g^*_3+g^*_6+g^*_7\right)+\frac{\eta}{\left|\partial\mathcal{D}_{\text{out}}\right|}				\\
\phantom{\bullet}\ P^{*,g}_5(\bgg^*,\bff )=g^*_5-\frac{1}{6}\left(\frac{1-3\eta}{1+3\eta}\right)\left(4g^*_3+g^*_6+g^*_7\right)+\frac{\eta}{\left|\partial\mathcal{D}_{\text{out}}\right|}				\\
\phantom{\bullet}\ P^{*,g}_8(\bgg^*,\bff )=g^*_8-\frac{1}{6}\left(\frac{1-3\eta}{1+3\eta}\right)\left(4g^*_3+g^*_6+g^*_7\right)+\frac{\eta}{\left|\partial\mathcal{D}_{\text{out}}\right|}	 			\\
\\
\bullet\ \forall\bx\in\partial\mathcal{D}_{\text{ns}}:   	\\
\phantom{\bullet}\ P^{*,f}_{(\boldsymbol{c}_{i}\cdot \boldsymbol{n} > 0)}(\bff^*)= f^*_{(\boldsymbol{c}_{i}\cdot \boldsymbol{n} > 0)}-f^*_{(\boldsymbol{c}_{\textcolor{red}{\bar{i}}}\cdot \boldsymbol{n} < 0)}=0,	\\
\phantom{\bullet}\ P^{*,g}_{(\boldsymbol{c}_{i}\cdot \boldsymbol{n} > 0)}(\bgg^*)= g^*_{(\boldsymbol{c}_{i}\cdot \boldsymbol{n} > 0)}-g^*_{(\boldsymbol{c}_{\textcolor{red}{\bar{i}}}\cdot \boldsymbol{n} < 0)}=0.
\end{array}\right.\end{equation}
%
with:
% 
\begin{equation}
\begin{array}{l}
f_{i}^{*,\text{eq}}=\sum_{j=0}^{8}{\frac{\partial f_{j}^{\text{eq}}}{\partial f_{i}}f_{j}^{*}}, \\
Q^{*,f}_{i}=\sum_{j=0}^{8}{-g_{j}^{*}\left(\frac{3\,\omega_{j}\,\conc}{\tau_{g}}\right)\boldsymbol{c}_{j}\cdot \boldsymbol{c}_{i}}, \\
g_{i}^{eq,*}=\sum_{j=0}^{8}{\frac{\partial g_{j}^{eq}}{\partial g_{i}}g_{j}^{*}}, \\
Q^{*,g}=\sum_{j=0}^{8}{\frac{\partial s_j}{\partial g_{i}}g_{j}^{*}},\\%=r\,k \exp(-r\,\conc)\sum_{j=0}^{8}{\omega_{j}g_{j}^{*}},\\
\eta=-\rho_{\text{out}}+f_{0}+f_{2}+f_{4}+2(f_{1}+f_{5}+f_{8}), \\
\zeta=\frac{6 \left(g_{1}+g_{5}+g_{8}\right)}{\left(1+3\eta\right)}.
\end{array}
\end{equation}

To compute the different adjoint-state variables, the steady-state solution of the forward problem is used. The adjoint-states being computed, the cost function gradient is finally computed through 
%
\begin{multline}\label{eq:costGrad} 
\nabla_\Psi \widehat{\mathcal{J}}^+\left(\bff,\bgg,\bff^*,\bgg^*\right)=\\
%
 -\alpha'(\Psi)\int_{0}^{t_{f}}{\,\sum_{i=0}^{8}{\omega_{i}f_{i}^{*}\left(\mathbf{M}^{-1}\mathbf{S}\mathbf{M} \left(3\,\boldsymbol{c}_{j}\cdot\boldsymbol{u}
+\frac{9}{2}(\boldsymbol{c}_{j}\cdot \boldsymbol{u})^{2}-\frac{3}{2}\boldsymbol{u}^{2}\right)\right)_i\,\md t}}\\
%
-\alpha'(\Psi)\int_{0}^{t_f} \sum_{i=0}^{8} \frac{3\,\omega_i\, g_i^*  \conc\, \boldsymbol{c}_{i}\cdot \boldsymbol{u}}{\tau_g}\,\md t.
%
\end{multline}

The general topology optimization algorithm is detailed in Algorithm~\ref{algo:topo}. The iterative algorithm involves a criterion based on the stabilization of the cost function value which is detailed in the next section.

\begin{algorithm}
	\KwIn{Level-set function $\Psi^{(0)}$\;}
	{Topology $\mathcal{T}^{(0)}$ through $\alpha^{(0)}$ (see~\cref{eq:Psi2alpha}), along with the smooth version~\cref{eq:Psi2alphaRegul} \;}
	\While{criterion  \eqref{eq:opt:criterion} not satisfied }
	{
		{Compute the Boltzmann variables $\bff$ and $\bgg$ solving~\cref{eq:fwPbLBM}\;}
		{Compute the cost function value $\widehat{\mathcal{J}}^+\left(\bff,\bgg\right)$ with~\cref{eq:costJplusTer}\;}
		{Compute the adjoint Boltzmann variables $\bff^*$ and $\bgg^*$ solving~\cref{eq:bwPbLBM}\;}
		{Compute the cost function gradient $\nabla_\Psi\widehat{\mathcal{J}}^+$ from~\cref{eq:costGrad}\;}
		{Update of the geometry : actualization of the level-set function with~\cref{eq:PsiEvol}\;}
	}
	\Return{Optimal topology $\mathcal{T}^{(*)}$\;}
	\caption{General topology optimization algorithm}
	\label{algo:topo}
\end{algorithm}

