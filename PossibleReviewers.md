#  Editeur associé en charge de l'article :
   Pierre Sagaut

# Rapporteurs possibles :

-  Mathias Krause (OpenLB)
   Karlsruhe Institute of Technology
   mathias.krause@kit.edu

- 	François Dubois
	CNAM PARIS

-	Okkels

-  Kentaro Yaji
   Department of Mechanical Engineering and Science, Kyoto University,
   yaji@mech.eng.osaka-u.ac.jp

-	Alexandersen, Joe
   Solid Mechanics, Department of Mechanical Engineering, Technical University of Denmark
   joealex@mek.dtu.dk

-  Georg Pingen
   Union University
   gpingen@uu.edu


# hilights

- Topology optimization of an advection-diffusion-reaction problem using the lattice Boltzmann method coupled with a Level-Set Method

- Continuous adjoint-state problem in space and time

- Utilization of the multiple relaxation time operator in LBM to improve the numerical stability and to solve optimization problems up to Re=1000

- A study on the influence of the major physical parameters (Reynolds, Peclet and Damkohler numbers) for the forward problem as well as for the optimization results

- Validation of the proposed topology optimization method via a crosscheck comparing different optimized geometries

# novelties

- the solution of reactive fluid flow topology optimization problems using the adjoint Lattice Boltzmann Method coupled with the Level-Set Method

- An improved LBM numerical stability with the multi-relaxation time operator compared to the single relaxation time model which allows us to study topology optimization problems in a large Reynolds number range (from 10 to 1000) and exhibits radically different behaviors regarding to the Reynolds number

- The characterization of the role played by the major parameters (Reynolds, Peclet and Damkholer numbers) involved in the advection-diffusion reaction problem on the forward problem and on the optimized results

