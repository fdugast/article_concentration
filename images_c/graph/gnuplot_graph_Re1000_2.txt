
 set terminal pdf
 set output "Re1000_graph2.pdf"


 set multiplot 

set size 0.72,0.72
set origin -0.15,0.07
set termoption enhanced
set tics scale 0.25
set size square
set xlabel "Iterations number" font " 'Helvetica ,9" offset 0,0.8
set xrange [0:250]
set xtics 0,50,250
set ytics offset 0,0
#set ytics ("-5{/Symbol \327}10^{-4}" 5e-4, "0" 0)
set y2tics offset 0,0
set y2range [-0.2:2]
set y2tics 0.5,0.5,2
set ylabel "mean uc_{out} (10^{-3})" font "Helvetica,9" textcolor rgb '#0060ad' offset 1.5,0
set y2label "{/Symbol D}p/{/Symbol D}p_{max} Ratio" font "Helvetica,9" textcolor rgb '#C11D19' offset -2,0
set xtics nomirror
set xtics font "Verdana,9" offset 0,0.5
set key font "Verdana,6"
#set format y '%.0s*10^%S'; 
#set key out vert
#set key top center
#unset key
set y2tics textcolor rgb '#C11D19' font "Helvetica,9"
set ytics nomirror
set ytics textcolor rgb '#0060ad' font "Helvetica,9"
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 1
set style line 2 \
    linecolor rgb '#C11D19' \
    linetype 1 linewidth 1 
plot "concentration_J_Re1000.txt" every ::1::900 using 1:(1000*$2) axes x1y1 with line linestyle 1 title "mean uc_{out}",\
     "concentration_J_Re1000.txt" every ::1::900 using 1:3 axes x1y2 with line linestyle 2 title "{/Symbol D}p/{/Symbol D}p_{max} Ratio"

unset ytics
unset y2tics
unset ylabel
unset y2label
unset xlabel

#################################
################################

set size 0.52,0.52
set origin 0.65,0

set title "Iteration 250" font "Helvetica,9" offset 0,-1
unset key
set tics scale 0.25
set view map
set size square
set xrange [0:199]
set yrange [0:199]
set xtics 20,40,180 font "Helvetica,6" offset 0,1
set ytics 20,40,180 font "Helvetica,6" offset 1,0
set palette defined (0 0.047 0.31 0.52, 1 1 0.5 0 ,4 0.8784 0.8784 0.8784)
unset colorbox
splot "data_Re1000_solide_it250.txt" matrix with image


set border
set size 0.52,0.52
set origin 0.43,0


set title "Iteration 150" font "Helvetica,9" offset 0,-1

unset key
set tics scale 0.25
set view map
set size square
set xrange [0:199]
set yrange [0:199]
set xtics 20,40,180 font "Helvetica,6" offset 0,1
set ytics 20,40,180 font "Helvetica,6" offset 1,0
set palette defined (0 0.047 0.31 0.52, 1 1 0.5 0 ,4 0.8784 0.8784 0.8784)
unset colorbox
splot "data_Re1000_solide_it150.txt" matrix with image

set border
set size 0.52,0.52
set origin 0.65,0.35


set title "Iteration 50" font "Helvetica,9" offset 0,-1

unset key
set tics scale 0.25
set view map
set size square
set xrange [0:199]
set yrange [0:199]
set xtics 20,40,180 font "Helvetica,6" offset 0,1
set ytics 20,40,180 font "Helvetica,6" offset 1,0
set palette defined (0 0.047 0.31 0.52, 1 1 0.5 0 ,4 0.8784 0.8784 0.8784)
unset colorbox
splot "data_Re1000_solide_it50.txt" matrix with image


set size 0.52,0.52
set origin 0.43,0.35

set title "Iteration 0" font "Helvetica,9" offset 0,-1

unset key
set tics scale 0.25
set view map
set size square
set xrange [0:199]
set yrange [0:199]
set xtics 20,40,180 font "Helvetica,6" offset 0,1
set ytics 20,40,180 font "Helvetica,6" offset 1,0
set palette defined (0 0.047 0.31 0.52, 1 1 0.5 0 ,4 0.8784 0.8784 0.8784)
unset colorbox
splot "data_Re1000_solide_it0.txt" matrix with image

unset multiplot
