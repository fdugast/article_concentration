DOC = article_concentration
LETTER=letterToEditor
LETTER2REVIEWER=LetterToReviewers_JCP2019

PDFLATEX = pdflatex

.PHONY: usage pdf clean proper 

help: usage

usage:
	@echo ""
	@echo "USAGE: "
	@echo ""
	@echo "pdf      - pdflatex"
	@echo "all      - pdflatex + bibtex + pdflatex(x2)"
	@echo "clean    - propre"
	@echo "proper   - Ne garde que les sources"
	@echo "tgz      - Tar et gunzip all in one with the date !"
	@echo ""


pdf: $(DOC).tex
	$(PDFLATEX)  $(DOC).tex	

all: $(DOC).tex
	$(PDFLATEX)  $(DOC).tex	
	bibtex $(DOC)
	$(PDFLATEX)  $(DOC).tex	
	$(PDFLATEX)  $(DOC).tex	
	$(PDFLATEX) $(LETTER).tex
	$(PDFLATEX) $(LETTER2REVIEWER).tex
	bibtex $(LETTER2REVIEWER)
	$(PDFLATEX) $(LETTER2REVIEWER).tex
	$(PDFLATEX) $(LETTER2REVIEWER).tex

clean:
	rm -f *~ Figure-sources/*~ Figure-sources/*.pdf Figure-sources/*.gz
	rm -f *.aux Figure-sources/*.aux 
	rm -f *.nls
	rm -f *.bbl
	rm -f *.blg
	rm -f *.glo
	rm -f *.gls
	rm -f *.idx
	rm -f *.ilg
	rm -f *.ind
	rm -f *.lof
	rm -f *.log Figure-sources/*.log Sections/*.log
	rm -f *.lot
	rm -f *.out
	rm -f *.toc
	rm -f *.nlo
	rm -f *.spl
	rm -f $(DOC).synctex.gz $(DOC).dvi $(DOC).ps
	rm -f $(LETTER).synctex.gz $(LETTER).dvi $(LETTER).ps
	rm -f $(LETTER2REVIEWER).synctex.gz $(LETTER2REVIEWER).dvi $(LETTER2REVIEWER).ps

proper:
	make clean
	rm -f $(DOC).pdf
	rm -f $(LETTER).pdf
	rm -f $(LETTER2REVIEWER).pdf
