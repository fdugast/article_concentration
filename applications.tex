

\section{Test cases and results} \label{sec:results}

For the validation of the proposed method, the reader is referred to a previous work of the authors~\cite{DUGAST2018} where a thermal fluid flow topology optimization case has been computed by following a similar procedure. The obtained results have 
been then compared with a benchmark test in literature and a study about the grid independency and the sensitivity of the algorithm to different initializations have also been conducted. \\    

The geometry and the configuration of the test cases are shown on~\cref{config_opt}. The 2D square domain is enclosed by no-slip walls. The gray layers, shown on~\cref{config_opt}, represent the fixed solid boundary, on which the no-slip condition is applied.
The cavity contains the fluid-solid distribution,  and the set of inlet/outlet is also schematically represented. At the inlet, $\partial \mathcal{D}_{\text{in}}$, the fluid enters with a parabolic velocity profile.

\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}[scale=0.5,decoration={markings,
		mark=at position 1/8 with {\node (A) {};},
		mark=at position 2/8 with {\node (B) {};},
		mark=at position 3/8 with {\node (C) {};},
		mark=at position 4/8 with {\node (D) {};},
		mark=at position 5/8 with {\node (E) {};},
		mark=at position 6/8 with {\node (F) {};},
		mark=at position 7/8 with {\node (G) {};}}]
	\draw[draw=white] (0.5,0.5) rectangle (9.5,9.5);
	\draw[fill=gray!40,densely dashed] (0,8) -- (0.5,8)-- (0.5,0.5) -- (9.5,0.5) -- (9.5,1) -- (10,1) -- (10,0)--(0,0) -- (0,8);
	\draw[fill=gray!40, densely dashed] (0,9) -- (0,10)-- (10,10) -- (10,2) -- (9.5,2) -- (9.5,9.5) -- (0.5,9.5) -- (0.5,9)--(0,9);
	%\draw[draw=black] (10,10) [thin] -- (10,0);
	\draw  (0,0) [thin] -- (10,0) ;
	\draw  (0,10) [thin] -- (10,10) ;
	\draw  (0,0) [thin] -- (0,8) ;
	\draw  (0,9) [thin] -- (0,10) ;
	\draw  (10,0) [thin,black] -- (10,1) ;
	%\draw[rectangle,fill=white, draw=white]  (9,1) -- (11,2) ;
	\draw  (10,2) [thin,black] -- (10,10) ;
	\node[below,color=black,text width=3.5cm, text badly centered] at (5,6.5){{ Unknown fluid/solid distribution}};
	\tikzstyle{config1}=[align=center, text width=2cm]
	\tikzstyle{BC}=[anchor=west,align=left,font=\scriptsize,text width=3.3cm]
	\draw (10,1.5) node[right]{\textcolor{black}{$\partial \mathcal{D}_{\text{out}}$}};
	\draw (0,8.5) node[left]{\textcolor{black}{$\partial \mathcal{D}_{\text{in}}$}};
	\pgfmathsetmacro{\vx}{1.3}
	\pgfmathsetmacro{\vy}{5.25}
	\pgfmathsetmacro{\DA}{26}
	\draw (-0.4,9.1) [{<[scale=0.5]}-{>[scale=0.5]}]  -- (-0.4,10);
	\draw (-0.4,9.55) node[left]{$20$};
	\draw (-0.4,0) [{<[scale=0.5]}-{>[scale=0.5]}]  -- (-0.4,7.9);
	\draw (-0.4,3.95) node[left]{$160$};
	\draw (0,10.5) [{<[scale=0.5]}-{>[scale=0.5]}]  -- (10,10.5);
	\draw (5,10.5) node[above]{$200$};
	\draw (9.5,-0.4) [{<[scale=0.5]}-{>[scale=0.5]}]  -- (10,-0.4);
	\draw (9.75,-0.4) node[below]{$10$};
	\end{tikzpicture}
	\caption{Schematic representation of the optimization problem configuration.}
	\label{config_opt}
\end{figure}

The convergence criterion for both the forward LBM problem and the adjoint-state LBM problem has been chosen to be:
%
\begin{equation} 
\frac{\left||a^{(n)}-a^{(n-10,000)}\right\|_\infty}{\left\|a^{(n-10,000)}\right\|_\infty}<10^{-4}
\quad\text{with}\quad
\left\|a^{(n)}\right\|_\infty=\max_{\substack{q=\{f,g\} \\ j=0,...,8 \\ i=1,...,N}} \left|q_j^{(n)}(x_i)\right|,
\end{equation}
%
in which the infinity norm is applied on the vector involving all the nine directions, and all spatial nodes, for both quantities, and the superscript $(n)$ stands for the LBM iteration count. Besides, the convergence criterion of the optimization problem is based on the stabilization of the cost function value along with ten successive iterations:
%
\begin{equation}\label{eq:opt:criterion}
\left|\frac{\mathcal{J}^{(k)}-\mathcal{J}^{(k-10)}}{\mathcal{J}^{(k-10)}}\right|<10^{-4},
\end{equation}
%
where $\mathcal{J}^{(k)}$ is the cost function value at the iteration count $k$. Calculations were performed on a NVIDIA Quadro K6000 GPU card for taking advantage of the LBM algorithm parallelism. Normally, for the adjoint-states calculation, one needs to store the macroscopic values of the forward problem, at each LBM iteration, which is prohibitive in terms of memory requirement. Only steady-state problems are considered here, so, as stated by \cite{Liu2014c}, the solution of the forward problem at final time can be used as the stationary source term for the whole adjoint-states calculation.

Regarding the reaction source term, instead of the simple linear model introduced previously in the adimensionnalised Damkohler number, a two-parameters model is chosen for a more tunable reaction:
%
\begin{equation}
s=k \left(1-\exp(-r \conc)\right),
\label{eq2}
\end{equation}

The coefficient $k$ controls the maximum reaction rate,  and $r$ is a positive user-defined parameter that controls the dependency to the concentration: a high value prescribes a quasi-independent concentration rate   whereas a small value tends to a linear model.

The numerical examples presented in this section use the following parameters:
%
\begin{itemize}
	\item the spatial discretization for the domain is $200\times200$ elements,
	\item the reaction rate coefficient  $k$ is equal to $2\times 10^{-5}$,
	\item the Peclet number Pe is \num{2000},
	\item {the initialization starts with the full fluid topology, i.e. with $\Psi(\boldsymbol x)>0$,}
	\item the iteration matrix $\bold{P}$ used for the level-set function evolution in~\cref{eq:PsiEvol} is chosen to be a diagonal matrix satisfying $\bold{P}_i=\sfrac{0.01}{\alpha'(\Psi_i)}$. The benefit of this choice is threefold: i) the derivative $\alpha'(\Psi)\ge0$ is no longer involved in the level-set function evolution, so even a discontinuous function $\alpha(\Psi)$ can be used; ii) the user-parameter $\epsilon$ is not to be pre-assigned to a given value, this parameter being no longer involved anywhere in the optimization process; iii) there is no more bias between the response of the forward model, i.e. the value of the cost function $\mathcal{J}$ which would need $\epsilon=0$, and its gradient, $\nabla_\Psi\mathcal{J}$, which would need $\epsilon\neq0$ for its existence condition. Next, the descent parameter 0.01 involved at the numerator of the iteration matrix, has been chosen following Dugast et al.~\cite{DUGAST2018}.
	\item  the maximal allowed pressure drop is twice the initial pressure drop (calculated at the first iteration with the full fluid geometry),
	\item  the relaxation times for the MRT are set to $s_e=s_\epsilon=s_q={0.6}$  and  $s_\nu=1/(3\nu+0.5)$.
	\item the reaction rate coefficient  $r$ is equal to $10$, which corresponds to a Damkholer number Da equal to \num{0.1}.
\end{itemize}


As the MRT model allows the calculation of higher Reynolds number flows compared to the SRT model, the first study described hereafter is based on the influence of the Reynolds number on the optimized topology. Three cases are presented : $\text{Re}=$\num{10}, $\text{Re}=$\num{100} and $\text{Re}=$\num{1000}.  It is to be noticed that although the forward and adjoint-state problems could have been solved with the single-relaxation time model, for both Re=10 and Re=100, the convergence of the LBM solver is not possible without a modification of the relaxation times $s_{0-6}$ for $\text{Re}=$\num{1000}.
This study shows that the introduction of the solid parts breaks the main fluid flow path going from the inlet to the outlet via the center of the domain into several fluid paths. This is true for the three tested Reynolds numbers. This path division participates to a better distribution of the fluid flow on the entire domain, especially on the top-right and bottom-left corners compared to the initial configuration. One can notice that the complexity of the flow paths increases with the Reynolds number.
Apart from advection, the forward problem involves also diffusion and reaction phenomena. Consequently, the second study deals with the influence of the mass diffusivity and of the reaction rate on the obtained topology. The modification of the mass diffusivity affects the balance between advection and diffusion and therefore the impact of the fluid flow velocity on the concentration. The reaction rate is used to control the speed on the reaction. The influence of a faster reaction will be studied.
With these two studies, the topology optimization method has been tested for different flow regimes and reactants. It shows its ability to produce specific optimized topologies for a variety of configurations.


\subsection{Influence of the Reynolds number}

The first study deals with the influence of the Reynolds number on the obtained optimized topology. The objective is the maximization of the reaction within the domain, this measurement being calculated at the outlet, see~\cref{eq:costJplusTer}.
For this study, the Peclet number is kept constant. As $\text{Pe}=\text{Re} \times \text{Sc}$, it means that the Schmidt number is also modified along with the Reynolds number.




\subsubsection{Case 1 : $\text{Re}=$\num{10}}

\Cref{fig:Re10} presents the results of the optimization process along with the iterations, for the first case, with $\textrm{Re}=$\num{10}. On the top and left-hand side is presented the evolution of both the cost function value (mean of~$u\times c_{\text{out}}$), and of the pressure drop ratio $\frac{\Delta p}{\Delta p_{\max}}$, along with the iteration count. On the right-hand side, is presented the evolution of the topology: $\mathcal T_{[\text{Re=10}]}^{(0)}$ at the iteration 0 (it is the initial guess), $\mathcal T_{[\text{Re=10}]}^{(30)}$ at the iteration 30, $\mathcal T_{[\text{Re=10}]}^{(50)}$ at the iteration 50, and  after reaching the stabilization of the cost function value, $\mathcal T_{[\text{Re=10}]}^{*}$, at the iteration 900.

From this figure it is seen that the solid parts are introduced only in the center of the domain, but not close to both the inlet and the outlet. That is the reason why the increase in the pressure drop ratio is not much, and the constraint on the pressure drop is by far fulfilled.  In fact, the introduction of the solid medium close to the center of the medium divides the flow in two distinct paths, as can be seen from~\cref{fig:Re10}, in the middle. This makes such that the fluid velocity is greater elsewhere, especially close to the bottom right and top left corners. The effect is that more reactant is consumed in these areas; this participates to the high decrease of the outlet concentration (see the bottom of~\cref{fig:Re10}).
Note that, from the initial guess to the optimized topology, the
cost function has decreased from $5.10^{-3}$ (LB unit) down to $1.96.10^{-3}$ (LB unit), i.e. by a factor more than 2.5.

\begin{figure}[htbp]
	\begin{center}
	\hspace{-0.8cm}	\includegraphics[width=.8\textwidth]{images_c/graph/Re10_graph.pdf}
	\end{center}
	\begin{center}
		\includegraphics[width=.75\textwidth]{images_c/Re10_vitesse2.pdf}
		\includegraphics[width=.75\textwidth]{images_c/concentration_Re10.pdf}
		\caption{Re=10. Top left: evolution of the cost function and of the pressure drop ratio along with the iteration count. Top right: the topology at iterations 0, 30, 50, and 900. Middle: module of the velocity field. Left: guess configuration (full fluid). Right: optimized topology.
			Bottom: concentration field. Left: guess configuration (full fluid). Right: optimized topology.
		}
		\label{fig:Re10}
	\end{center}
\end{figure}



\subsubsection{Case 2 : $\text{Re}=$\num{100}}

\Cref{fig:Re100} presents the results of the optimization process along with the iterations, for the second case, with $\textrm{Re}=$\num{100}. On the top and left-hand side is presented the evolution of both the cost function value (mean of~$u\times c_{\text{out}}$), and of the pressure drop ratio $\frac{\Delta p}{\Delta p_{\max}}$, along with the iteration count. On the right-hand side, is presented the evolution of the topology: $\mathcal T_{[\text{Re=100}]}^{(0)}$ at the iteration 0 (it is the initial guess), $\mathcal T_{[\text{Re=100}]}^{(20)}$ at the iteration 20, $\mathcal T_{[\text{Re=100}]}^{(50)}$ at the iteration 50, and  after reaching the stabilization of the cost function value, $\mathcal T_{[\text{Re=100}]}^{*}$, at the iteration 390.
Comparing~\cref{fig:Re100} with~\cref{fig:Re10}, it can be seen that the decrease of the cost function is more important for Re=100 than for Re=10. Here, the cost function has decreased from $6.1.10^{-3}$ (LB unit) down to $5.96.10^{-4}$ (LB unit), i.e. by a factor greater than 10 .

Conversely to the previous case with Re=10, the optimal fluid/solid configuration is here composed of several parts, located near the diagonal of the domain, facing the main fluid flow stream. The fluid flow is thus mainly directed towards the corners, but also a part of it goes through the center of the domain, as the solid line is not continuous, containing some holes, see the plot in the middle of~\cref{fig:Re100}.

The outlet concentration for the optimized geometry is very close to zero, as can be seen from the plot in the bottom of~\cref{fig:Re100}. This means that almost all the reactant has been consumed inside the domain. In that sense, the efficiency of the chemical reaction has been highly increased.



%\begin{figure}[htbp]
%	\begin{center}
%		\includegraphics[width=\textwidth]{images_c/graph/Re100_graph.pdf}
%		\caption{Re=100. Left: evolution of the cost function and of the pressure drop ratio along the iteration count. Right: the topology at iterations 0, 20, 50, and 390.}
%		\label{J_Re100}
%	\end{center}
%\end{figure}
%
%\begin{figure}[htbp]
%\begin{center}
%\includegraphics[width=\textwidth]{images_c/vitesse_Re100.pdf}
%\caption{Re=100. Module of the velocity field. Left: guess configuration (full fluid). Right: optimized topology.}
%\label{u_Re100}
%\end{center}
%\end{figure}
%
%\begin{figure}[htbp]
%\begin{center}
%\includegraphics[width=\textwidth]{images_c/concentration_Re100.pdf}
%\caption{Re=100. concentration field. Left: guess configuration (full fluid). Right: optimized topology.}
%\label{c_Re100}
%\end{center}
%\end{figure}



\begin{figure}[htbp]
	\begin{center}
			\hspace{-0.8cm}
		\includegraphics[width=.8\textwidth]{images_c/graph/Re100_graph.pdf}
	\end{center}
	\begin{center}
	
				\includegraphics[width=.75\textwidth]{images_c/Re100_vitesse2.pdf}
		\includegraphics[width=.75\textwidth]{images_c/concentration_Re100.pdf}
		\caption{Re=100. Top left: evolution of the cost function and of the pressure drop ratio along with the iteration count. Top right: the topology at iterations 0, 20, 50, and 390. Middle: module of the velocity field. Left: guess configuration (full fluid). Right: optimized topology.
			Bottom: concentration field. Left: guess configuration (full fluid). Right: optimized topology.
		}
		\label{fig:Re100}
	\end{center}
\end{figure}







%\clearpage
\subsubsection{Case 3 : $\text{Re}=$\num{1000}}

In this last test case about the Reynolds number study, the velocity and concentration fields for $\text{Re}=1000$ are radically different to previous cases, with $\text{Re}=10$ and $\text{Re}=100$.
 As a matter of fact, the~\cref{Re1000_fluide} is to be compared to~\cref{fig:Re10} and~\cref{fig:Re100}, middle and left-hand side for the velocity, and bottom and left-hand side for the concentration, at the initial guess, i.e. for the full fluid flow case.
In fact, when $\text{Re}=1000$,
a re-circulation zone appears around the center of the domain. As a consequence, the concentration is very small there at the center of the vortex, but the concentration remains very high at the outlet.

The fluid/solid configuration found by the implemented topology optimization algorithm is, indeed, very far away from an optimized one, as the optimization process is struggling to find any proper topology. In order to facilitate the convergence of the optimization problem, the initial configuration was set to a uniform distribution of small solid circles inside the domain. With such an initialization, the re-circulation disappears, and the research of optimized geometries is easier. Note that the cost function value for the full fluid geometry was $12.6.10^{-3}$ (LB unit) while it is now $4.5.10^{-3}$ (LB unit) with this initialization, i.e. with circles.

The evolution along with the optimization iterations of the cost function and of the pressure drop limitation are shown on~\cref{fig:Re1000}, for this case. At the beginning of the optimization process, the pressure drop increases. This is due to the fact that some solid parts are introduced at the outlet of the domain. This may be seen as a drawback, but in fact, this also participates to the decrease of the concentration field at the outlet, which is the main objective of the optimization problem. Then, after some iterations, the limitation of the pressure drop included in the augmented cost function helps to prevent from the introduction of more solid parts near this area. The different solid parts introduced inside the domain are found to be very small: it is even difficult to see the difference between the different optimization steps, without a close look. But these small modifications have a huge impact on the cost function decrease.

As can be seen on~\cref{fig:Re1000}, a fluid path has been drawn on the left-hand side of the domain, by removing some solid elements from the initial topology. The distribution of the reactant is then more uniform, and the reaction rate is more important in this area, which participates to a diminution of the outlet concentration, as can be seen on the plot on the bottom of~\cref{fig:Re1000}. In that case again, the efficiency of the chemical reaction has been highly increased.

\begin{figure}[htbp]
\begin{center}
	\includegraphics[width=.8\textwidth]{images_c/Re1000_fluide.pdf}
	\caption{Re=1000. Velocity (left) and concentration (right) fields for the full fluid topology.}
	\label{Re1000_fluide}
\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		\hspace{-1cm}
		\includegraphics[width=.8\textwidth]{images_c/graph/Re1000_graph.pdf}
			\end{center}
	\begin{center}
		\includegraphics[width=.75\textwidth]{images_c/Re1000_vitesse2.pdf}
		\includegraphics[width=.75\textwidth]{images_c/concentration_Re1000.pdf}
		\caption{Re=1000. Top left: evolution of the cost function and of the pressure drop ratio along with the iteration count. Top right: the topology at iterations 0, 50, 150, and 250. Middle: module of the velocity field. Left: guess configuration. Right: optimized topology.
		Bottom: concentration field. Left: guess configuration. Right: optimized topology.
		}
		\label{fig:Re1000}
	\end{center}
\end{figure}


\subsubsection{Crosscheck of the different optimized configurations}

Based on the Reynolds number test cases, a crosscheck comparison of the different optimal topologies has been performed in order to discuss on the results. 
 In this additional study, the fluid flow with Reynolds numbers equal to 10, 100, and 1000 is applied on the three previously obtained optimal topologies. The cost function measuring the reaction efficiency  is then calculated for each of these nine cases.
The results are reported in~\cref{crosscheck}. The first row is related to the three different found optimal topologies, and the first column is related to the Reynolds number which is applied. The other numbers are the values of the calculated cost function.

\begin{table}[h]
	\centering
	\begin{tabular}{l | c c r}
		& $\mathcal T_{[\text{Re=10}]}^*$ & $\mathcal T_{[\text{Re=100}]}^*$ & $\mathcal T_{[\text{Re=1000}]}^*$ \\
		\hline
		$\text{Re}=$\num{10} & 1.9 & \textbf{0.8} & 2.6 \\
		$\text{Re}=$\num{100} & 2.3 & \textbf{0.59} & 2.6 \\
		$\text{Re}=$\num{1000} & 12.6 & 8.4 & \textbf{0.49} \\
	\end{tabular}
	\caption{Cost function values $(\times10^{-3})$ for different configurations}
	\label{crosscheck}
\end{table}

The expected result is that the minimum value of the cost function is to be found for the applied Reynolds number equal to the one related to its optimal geometry. For readability considerations, the minimum value  of the cost function for each Reynolds number is written in bold characters, in~\cref{crosscheck}.

For the applied Reynolds number equal to 100, one can see that the related optimized topology, $\mathcal T_{[\text{Re=100}]}^*$, provides, by far, the best result. The same trend is observed for the applied Reynolds number equal to 1000, but with even a higher difference between the three topologies: the cost function is equal to 0.49   with the topology $\mathcal T_{[\text{Re=1000}]}^*$, while it is equal to 8.4 and 12.6, for topologies $\mathcal T_{[\text{Re=100}]}^*$ and $\mathcal T_{[\text{Re=10}]}^*$, respectively.

However, for the applied Reynolds number equal to 10, it is seen from~\cref{crosscheck} that the minimum cost function value is given for the topology $\mathcal T_{[\text{Re=100}]}^*$. This highlights that the topology obtained at the end of the optimization process, after convergence, for the case of $\text{Re}=10$, is a local minimum. 
Even if this crosscheck did not show a local minimum for the two other configurations, it is to be noticed that this does not mean that the global minimum has been actually reached.
In fact, it is well known that gradient-type methods, because they are based on a local study of the function to be minimized, usually reach a local minimum next to the initial guess~\cite{Bruns2007a}. This is usually considered as the main drawback of these gradient-type algorithms. But, at the same time, compared to gradient-free optimization methods --~which, indeed, are more likely to find the global minimum, gradient-type algorithms reach the minima much more efficiently. This point makes the difference. 
In fact, the optimization problem treated here involves \num{200}$\times$\num{200} unknowns; such a discretization would be impracticable with any gradient-free optimizer. 
This difficulty could of course be overcome by modifying the initial topology, many times, but, as the physical problem is complex, it will remain difficult (and likely impossible) to find the unique global minimum.

In further perspectives, the local minimum treatment is an interesting issue to solve, despite the fact that it can be difficult to avoid them while using a gradient-based optimization method. Nevertheless, one possibility would be to use a multi-scale resolution approach. Such a multi-resolution approach, formalized by \textcolor{blue}{Liu}~\cite{liu1993multiresolution} for solving some ill-posed inverse problems,  has been later on successfully used by Dubot et al.~\cite{dubot2015wavelet} and Liu~\cite{liu2018wavelet}, for example. In the framework of reactive fluid flow topology optimization, the use of a multi-scale resolution approach would enable to perform a scale-by-scale optimization on successive convex cost functions, following the work of Chavent~\cite{chavent2010nonlinear}. Moreover, added to this interesting property, this would also save computational time due to faster convergence. This point will be addressed in a future research.

Following the study of the Reynolds number, the influence of both the reaction rate and diffusion is investigated. Additionally, the Damkholer and Peclet number are modified to determine their effect on the resultant system geometry.


\subsection{Case 4 : Influence of the Damkohler number}
\label{subsection:Da}

  The reaction rate $k$ is now modified in order to evaluate the influence of the Damkohler number on the optimal topology. It has been increased from $2\times 10^{-5}$ to $4\times 10^{-5}$. Though this modification does not have any impact on the velocity, the concentration field is altered, as the reactant is consumed faster. This effect is clearly visible by comparing~\cref{fig:Da,fig:Re1000}.

  Concerning the optimization results, one can observe a similar trend between this case and the previous case 3. First, the evolution of the cost function and the pressure drop ratio, represented on the top of~\cref{fig:Da}, are similar: one observes an increase and then a decrease of the pressure drop ratio, while the outlet concentration is decreasing. The optimal topology is also comparable to the one of case 3: it implies the same modification on the velocity in which the flow is divided into two main streams just after the inlet. The main difference between both the previous case 3 and this case 4 is in the magnitude of the outlet concentration. This one is much smaller for the higher Damkholer number, as it was expected.
  
  One can also see from~\cref{comp_Da} (left) that the solid part at the core of the domain is denser for Da=0.1 (in red) than for Da=0.2 (in blue). Please note that, to enhance readability, the solid parts have been represented larger (in this figure only). 
  %
  For Da=0.2, because the reactant is consumed faster (with respect to Da=0.1), it is consequently easier to obtain a small concentration at the outlet, even without the need to alter the velocity field through the addition of significant solid material.
  %
  The comparison of the concentration field for the two Damkholer numbers is shown on~\cref{comp_Da} (right). The threshold $c_A\gtrless0.05$ is applied to the concentration field to get a better contrast between the two cases. For the higher Damkholer number, one can see that the concentration becomes very low shortly after the middle of the domain. As the reactant has been almost entirely consumed, the efficiency has been highly increased regarding to the cost function. However, the other conclusion that can be made based on this result is that the reactor has been over-sized for this reaction rate. In fact, based on the concentration field obtained for the optimized geometry, one can conclude that a good efficiency could have also been achieved with a smaller reactor. The real cost of the chemical reactor and the dimensions associated with it were not included in the optimization problem; it is nevertheless a useful conclusion from an engineering point of view.


\begin{figure}[htbp]
\begin{center}
%\includegraphics[width=0.8\textwidth]{images_c/graph/Re1000_Da_graph.pdf}
\hspace{-1cm}
\includegraphics[width=0.8\textwidth]{images_c/graph/Da_2.pdf}
	\end{center}
\begin{center}
\includegraphics[width=0.75\textwidth]{images_c/vitesse_Da.pdf}
	\end{center}
\begin{center}
\includegraphics[width=0.75\textwidth]{images_c/concentration_Da.pdf}
\caption{$\text{Da}=4\times 10^{-5}$. Top left: evolution of the cost function and of the pressure drop ratio along with the iteration count. Top right: the topology at iterations 0, 100, 200, and 340. Middle: module of the velocity field. Left: guess configuration (full fluid). Right: optimized topology. Bottom: concentration field. Left: guess configuration (full fluid). Right: optimized topology.}
\label{fig:Da}
\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=\textwidth]{images_c/graph_comp_Da.pdf}
		\caption{Comparison of optimization cases for Da=0.1 and Da=0.2: optimal topologies (left) and concentration levels (right)}
		\label{comp_Da}
	\end{center}
\end{figure}

\subsection{Case 5 : Influence of the Peclet number}

In the previous numerical examples, the Peclet number was equal to 2000. The advection process played a major role compared to the diffusion process.
In this test case 5, the Peclet number is modified to be equal to 100. For the rest, the configuration is kept the same as for the previous case 3, and the Reynolds number is 1000. Note that in this case, the diffusion process is no longer negligible in the chemical reaction. Concerning the forward problem, the velocity field is not altered by this modification. However, important changes are observed in the concentration field.
The pressure drop ratio is important for this case, as can be seen from~\cref{fig:Pe}, on the top left. This can be explained by the presence of solid parts near both the inlet and the outlet of the domain.
Similarly to previous examples, the flow is divided into two paths near the inlet.
With the initial topology, the reactant was spread from the inlet towards only the top-right corner of the domain.
With the optimal topology, an equilibrium has been found between the propagation of the reactant on both the top-right corner and the bottom-left corner: a symmetry of the concentration can be observed, from the inlet down to the outlet. One can notice that, to obtain this behavior, the fluid flow is progressively divided into smaller channels from the inlet to the center of the domain. Indeed, the density of solid parts is the most important on the diagonal connecting the bottom-left corner to the top-right corner. With such a pattern, the minimization of the outlet concentration is well achieved.

\begin{figure}[htbp]
\begin{center}
	\hspace{-0.8cm}
\includegraphics[width=0.8\textwidth]{images_c/graph/Re1000_Pe01_graph.pdf}
	\end{center}
\begin{center}
\includegraphics[width=0.75\textwidth]{images_c/Pe_vitesse2.pdf}
\includegraphics[width=0.75\textwidth]{images_c/concentration_Pe01.pdf}
\caption{Pe=100. Top left: evolution of the cost function and of the pressure drop ratio along with the iteration count. Top right: the topology at iterations 0, 50, 100, and 150. Middle: module of the velocity field. Left: guess configuration (full fluid). Right: optimized topology.
	Bottom: concentration field. Left: guess configuration (full fluid). Right: optimized topology.
}
\label{fig:Pe}
\end{center}
\end{figure}


\subsection{Overview and analysis of the different results}

The characteristics of the optimized geometries obtained for all cases are summarized in~\cref{summary}. 

%For the Reynolds number study, the Damkholer number is 0.1 and the Peclet number is 2000. 
Among all cases, the decrease of the cost function is less important for Re=10 (case 1) and for Pe=100 (case 5). In these two cases, either the advection is the smallest (Re=10, case 1), or the diffusion is the greatest (Pe=100, case 5), and the consequences are the same: the impact of the velocity field on the concentration is reduced, and so is the impact of the topology on the chemical reaction efficiency. 

For the study on the Damkholer number, the reaction rate is multiplied by~2 (from the test case 3 to the test case 4), and the impact of this modification is consequent on the final value of the cost function. The initial value was also lower than the other cases, about one order of magnitude compared to the case 3. As it has been stated in~\cref{subsection:Da}, the major conclusion regarding the optimized geometry obtained for this case is not only the efficiency of the reaction, but rather the over-sizing of the reactor. In~\cref{comp_Da}, one can see that there are less solid parts in the core of the domain for $\text{Da}=$\num{0.2} than for $\text{Da}=$\num{0.1}. The porosity value in~\cref{summary} does not reflect this, but this can be explained by the fact that for $\text{Da}=$\num{0.2}, the biggest solid part (which is curved and close to the inlet) represents alone 0.37~\% of the total porosity.

From~\cref{summary}, it is seen that the pressure drop ratio slightly increases for Re=10 (case 1) and for Re=100 (case 2), but this pressure drop decreases for Re=1000 (case 3) and Da=0.2 (case 4). In all cases, as the maximal pressure drop is prescribed to twice the pressure drop given for the initial geometry, the initial pressure drop ratio is equal to 0.5. So, for the two first cases, any added solid within the void medium would increase this pressure drop ratio. However, as the initialization for Re=1000 is realized with a uniform distribution of small circles, the pressure drop can be smaller to the initial one if some circles are removed, especially near the inlet and near the outlet of the domain. This phenomenon happens for Re=1000 (case 3) and for Da=0.2 (case 4). For the test case 5 (study on the Peclet number), the initialization also involves circles, but, as some additional solid parts are introduced, especially near the inlet and near the outlet, this participates to an increase of the pressure drop ratio. As it can be seen on the different figures concerning this study, the porosity for a lower Peclet number is higher than for the other cases. The optimization domain is composed of more solid parts to have a stronger control on the velocity field, useful to counter-balance the higher diffusivity, but this is costly in term of pressure drop. One can notice that in all the cases, the optimization process is achieved by the addition of a small amount of materials (98.26~\%  of minimal final porosity, except for the low Pe number test). This last configuration is characterized by a lower porosity (97.1~\%, which corresponds to three times more added material). The term porosity not only explains the amount of material but also its structure. Indeed, except at the lowest Re number, where optimal form acts as a profile by splitting the initial major flow, the optimal solid distributions look like porous media. In both the Re=100 case and low Pe number case, a plane structure appears in the diagonal of the cavity, perpendicular to the line joining the entrance and the exit. This structure acts as a perforated plane with adjusted holes in order to control and to balance the fluid flow. 
 
In all these configurations, the optimization algorithm has been able to design different topologies able to increase the efficiency of the chemical reactors, showing its adaptability and usefulness working with several physical parameters involved in a multiphysics problem.

\begin{table}[h]
	\centering
	\begin{tabular}{c | c c c c | c c c c}
		Case \# & Da&Re&Pe &$\mathcal{J}^{*}$ & $\mathcal{J}^{(0)}/ \mathcal{J}^{*}$& $\Delta p$ ratio & Porosity  \\
		\hline
		1&0.1&10&2000 & 1.9 & 2.60 & 0.53 & 98.72\% \\
		2&0.1&100&2000 & 0.59 & 10.40  & 0.61 & 98.26\% \\
		3&0.1&1000&2000 & 0.49 & 9.08 & 0.33 & 98.85\% \\
			\hline
		4&0.2&1000&2000 & 0.012 & 62.50 & 0.306 & 98.75\% \\
			\hline
		5&0.1&1000&100 & 0.72 &  2.36 & 1.03 & 97.1\% \\
	\end{tabular}
	\caption{Optimization results for different configurations}
	\label{summary}
\end{table}

